<?php

namespace Drupal\wellknown\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures settings for .well-known paths.
 */
class WellKnownSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['wellknown.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'wellknown_settings_form';
  }

  /**
   * Builds the form.
   *
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('wellknown.settings');

    $form['wellknown_paths'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('Value'),
        $this->t('Operations'),
      ],
      '#prefix' => '<div id="wellknown-paths-wrapper">',
      '#suffix' => '</div>',
    ];

    $paths = $form_state->getValue('wellknown_paths') ?? $config->get('paths') ?? [];

    $paths = self::filterPaths($paths);

    $paths[] = ['name' => '', 'value' => ''];

    foreach ($paths as $key => $path) {
      $form['wellknown_paths'][$key]['name'] = [
        '#type' => 'textfield',
        '#default_value' => $path['name'],
      ];
      $form['wellknown_paths'][$key]['value'] = [
        '#type' => 'textarea',
        '#default_value' => $path['value'],
      ];
      $form['wellknown_paths'][$key]['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::ajaxCallback',
          'wrapper' => 'wellknown-paths-wrapper',
        ],
        '#name' => 'remove_' . $key,
      ];
    }

    $form['actions']['add_path'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add new path'),
      '#submit' => ['::addPath'],
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'wrapper' => 'wellknown-paths-wrapper',
      ],
    ];

    return $form;
  }

  /**
   * AJAX callback handler for Add/Remove operations.
   */
  public function ajaxCallback(array &$form, FormStateInterface $form_state): array {
    return $form['wellknown_paths'];
  }

  /**
   * Submit handler for the "add path" button.
   */
  public function addPath(array &$form, FormStateInterface $form_state): void {
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove" button.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state): void {
    $triggering_element = $form_state->getTriggeringElement();
    $button_name = $triggering_element['#name'];
    $index_to_remove = substr($button_name, 7);

    $paths = $form_state->getValue('wellknown_paths');
    unset($paths[$index_to_remove]);
    $form_state->setValue('wellknown_paths', $paths);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $paths = $form_state->getValue('wellknown_paths');

    $paths = array_map(function($path) {
      return [
        'name' => $path['name'],
        'value' => $path['value'],
      ];
    }, $paths);

    $paths = self::filterPaths($paths);

    $this->config('wellknown.settings')
      ->set('paths', $paths)
      ->save();

    parent::submitForm($form, $form_state);

    // Invalidate the route cache to ensure new routes are registered.
    \Drupal::service('router.builder')->rebuild();
  }

  /**
   * Filters out empty paths
   */
  protected static function filterPaths(array $paths): array {
    return array_filter($paths, function($path) {
      return !empty($path['name']) && !empty($path['value']);
    });
  }

}
