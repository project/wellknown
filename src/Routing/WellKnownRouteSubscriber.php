<?php

namespace Drupal\wellknown\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Defines dynamic routes for .well-known URIs.
 */
class WellKnownRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    $config = \Drupal::config('wellknown.settings');
    $paths = $config->get('paths');
    if (!empty($paths)) {
      foreach ($paths as $path) {
        $route = new Route(
          '/.well-known/' . $path['name'],
          [
            '_controller' => '\Drupal\wellknown\Controller\WellKnownController::response',
            'content' => $path['value'],
          ],
          ['_access' => 'TRUE']
        );
        $collection->add('wellknown.' . $path['name'], $route);
      }
    }
  }

}
