<?php

namespace Drupal\wellknown\Controller;

use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for returning responses for .well-known URIs.
 */
class WellKnownController {

  /**
   * Returns a response for a .well-known URI.
   */
  public function response($content): Response {
    return new Response($content);
  }

}
